// Copyright Epic Games, Inc. All Rights Reserved.

#include "FXGameMode.h"
#include "FXCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFXGameMode::AFXGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
